Dans le terminal à la racine du projet, exécuter la commande suivante :

docker-compose up -d --build

Ouvrir le cli du container mongo_beer_api.

Lancer le cli mongo Executer les commandes suivantes:

  mongo

  use beer_appetit_api

  db.roles.insertMany([{ name: "ROLE_USER" },{ name: "ROLE_MEMBER" },{ name: "ROLE_ADMIN" }])

db.style.insertMany([{name:"american amber ale"},{name:"american barleywine"},{name:"american brown ale"},
{name:"american ipa"},{name:"american pale ale"},{name:"american porter"},{name:"american stout"},
{name:"american strong ale"},{name:"baltic porter"},{name:"belgian blond ale"},{name:"belgian golden strong ale"},
{name:"belgian ipa"},{name:"belgian pale ale"},{name:"berliner weisse"},{name:"bière de garde"},{name:"bitter"},
{name:"black ipa"},{name:"blond ale"},{name:"british strong ale"},{name:"california common"},{name:"dark strong ale"},
{name:"doppelbock"},{name:"dortmunder_export"},{name:"double ipa"},{name:"dubbel"},{name:"Dunkles Weissbier"},
{name:"eisbock"},{name:"english barleywine"},{name:"english ipa"},{name:"english porter"},
{name:"extra special bitter"},{name:"faro"},{name:"festbier - oktoberfest"},{name:"flandres red"},
{name:"foreign extra stout"},{name:"fruit beer"},{name:"golden ale"},{name:"gose"},{name:"gruit"},
{name:"gueuze"},{name:"hefeweizen"},{name:"imperial porter"},{name:"imperial stout"},{name:"International Pale Ale"},
{name:"irish red ale"},{name:"irish stout"},{name:"Italian Grape Ale"},{name:"kolsch"},{name:"lager"},
{name:"lambic - Fruit"},{name:"milk - sweet stout"},{name:"munich hells"},{name:"neipa - milkshake IPA"},
{name:"oatmeal stout"},{name:"old ale"},{name:"oud bruin"},{name:"pilsner"},{name:"quadrupel"},
{name:"rauchbier"},{name:"saison - farmhouse"},{name:"schwarzbier"},{name:"scotch ale - wee heavy"},
{name:"scottish ale"},{name:"session ipa"},{name:"smoked beer"},{name:"specialty grain - rye"},
{name:"spice"},{name: "vegetable"},{name:"tripel"},{name:"vienna lager"},{name:"weizenbock"},
{name:"wheat beer"},{name:"white ipa"},{name:"wild - sour beer"},{name:"winter ale - christmas ale"},
{name:"witbier"}])

db.color.insertMany([{name:"ambrée"},{name:"blanche"},{name:"blonde"},{name:"brune"},{name:"noire"},{name:"rosée"},{name:"rubis"},{name:"verte"}])
