package com.example.beerappetit.controllers;

import com.example.beerappetit.dtos.user.GetProfilDto;
import com.example.beerappetit.models.Beer;
import com.example.beerappetit.models.User;
import com.example.beerappetit.services.impl.UserServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("${beer_appetit.url}/users")
@Api(value="Users",tags = {"Users"})
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping()
    public List<User> findAll(){return this.userService.findAll();}

    @GetMapping("{id}")
    public GetProfilDto findById(@PathVariable String id){
        return  this.userService.findById(id);
    }

    @PatchMapping("{id}/email")
    public User updateEmail(@PathVariable String id, @RequestBody String email)
    {
        return this.userService.updateEmail(id, email);
    }

    @PostMapping("{userId}/beers/favoris")
    public List<Beer> setfavoris(@PathVariable String userId, @RequestBody Beer beer){
       return this.userService.setFavoris(userId,beer);
    }

    @GetMapping("{userId}/beers/favoris")
    public List<Beer> getFavoris(@PathVariable String userId){
        return  this.userService.getFavoris(userId);
    }

    @GetMapping("{userId}/beers/favoris/count")
    public int getCountFavoris(@PathVariable String userId){
        return this.userService.getCountFavori(userId);
    }

    @PostMapping("{userId}/beers/shoplist")
    public List<Beer> setShopList(@PathVariable String userId, @RequestBody Beer beer){
        return this.userService.setShopList(userId,beer);
    }

    @GetMapping("{userId}/beers/shoplist")
    public List<Beer> getShopList(@PathVariable String userId){
        return  this.userService.getShopList(userId);
    }

}
