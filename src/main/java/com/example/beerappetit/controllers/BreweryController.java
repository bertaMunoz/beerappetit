package com.example.beerappetit.controllers;

import com.example.beerappetit.models.Brewery;
import com.example.beerappetit.services.impl.BeerServiceImpl;
import com.example.beerappetit.services.impl.BreweryServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("${beer_appetit.url}/breweries")
@Api(value="onlinestore", description="BREWERY test")
public class BreweryController {

    @Autowired
    private BreweryServiceImpl breweryService;

    @GetMapping
    public List<Brewery> findAll() {
        return this.breweryService.findAll();
    }

    @GetMapping("{id}")
    public Brewery findById(@PathVariable String id) {
        return this.breweryService.findById(id);
    }

    @PostMapping
    public Brewery create(@RequestBody Brewery brewery) {
        return this.breweryService.create(brewery);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") String id) {

        this.breweryService.deleteById(id);
    }

    @PutMapping("{id}")
    public Brewery update(@RequestBody Brewery brewery) {
        return this.breweryService.update(brewery);
    }
}
