package com.example.beerappetit.controllers;

import com.example.beerappetit.models.Brewery;
import com.example.beerappetit.models.Color;
import com.example.beerappetit.models.Style;
import com.example.beerappetit.services.impl.BreweryServiceImpl;
import com.example.beerappetit.services.impl.ColorServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("${beer_appetit.url}/colors")
@Api(value="onlinestore", description="COLOR test")
public class ColorController {

    @Autowired
    private ColorServiceImpl colorService;


    @GetMapping
    public List<Color> findAll() {
        return this.colorService.findAll();
    }

    @GetMapping("{id}")
    public Color findById(@PathVariable String id) {
        return this.colorService.findById(id);
    }

    @PostMapping
    public Color create(@RequestBody Color color) {
        return this.colorService.create(color);
    }

    @PutMapping("{id}")
    public Color update(@RequestBody Color color, @PathVariable String id ) {
        return this.colorService.update(color);
    }
    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        this.colorService.deleteById(id);
    }



}
