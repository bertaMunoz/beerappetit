package com.example.beerappetit.controllers;

import com.example.beerappetit.models.Food;
import com.example.beerappetit.services.impl.FoodServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("beer_appetit/foods")
@Api(value="foods",tags = {"Foods"})
public class FoodController {

    @Autowired
    private FoodServiceImpl service;

    @GetMapping
    public List<Food> findAll(){
        return this.service.findAll();
    }

    @GetMapping("{id}")
    public Food findById(@PathVariable String id){
        return this.service.findById(id);
    }

    @PostMapping
    public Food create(@RequestBody Food dto){
        return this.service.create(dto);
    }

    @PutMapping("{id}")
    public Food update(@RequestBody Food dto){
        return this.service.update(dto);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id){
        this.service.deleteById(id);
    }

}
