package com.example.beerappetit.controllers;

import com.example.beerappetit.models.Brewery;
import com.example.beerappetit.models.Country;
import com.example.beerappetit.services.impl.BreweryServiceImpl;
import com.example.beerappetit.services.impl.CountryServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("${beer_appetit.url}/countries")
@Api(value="onlinestore", description="COUNTRY test")
public class CountryController {

    @Autowired
    private CountryServiceImpl countryService;

    @GetMapping
    public List<Country> findAll() {
        return this.countryService.findAll();
    }

    @GetMapping("{id}")
    public Country findById(@PathVariable String id) {
        return this.countryService.findById(id);
    }

    @PostMapping
    public Country create(@RequestBody Country country) {
        return this.countryService.create(country);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        this.countryService.deleteById(id);
    }

    @PutMapping("{id}")
    public Country update(@RequestBody Country country) {
        return this.countryService.update(country);
    }


}
