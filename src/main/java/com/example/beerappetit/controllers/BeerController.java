package com.example.beerappetit.controllers;

import com.example.beerappetit.models.Beer;
import com.example.beerappetit.services.impl.BeerServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin
@RequestMapping("${beer_appetit.url}/beers")
@Api(value="Beers",tags = {"Beers"})
public class BeerController {

    @Autowired
    private BeerServiceImpl beerService;

    @GetMapping
    public List<Beer> findAll() {
        return  this.beerService.findAll();
    }

    @GetMapping("{id}")
    public Beer findById(@PathVariable String id) {
        return this.beerService.findById(id);
    }

    @PostMapping
    public Beer create(@RequestBody Beer dto) {
        return (Beer) this.beerService.create(dto);
     }

    @PutMapping("{id}")
    public Beer update(@RequestBody Beer dto) {
        return this.beerService.update(dto);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        this.beerService.deleteById(id);
    }

}
