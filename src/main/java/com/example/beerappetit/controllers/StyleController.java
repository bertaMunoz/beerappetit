package com.example.beerappetit.controllers;

import com.example.beerappetit.models.Country;
import com.example.beerappetit.models.Style;
import com.example.beerappetit.services.impl.CountryServiceImpl;
import com.example.beerappetit.services.impl.StyleServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("${beer_appetit.url}/styles")
@Api(value="onlinestore", description="STYLE test")
public class StyleController {

    @Autowired
    private StyleServiceImpl styleService;

    @GetMapping
    public List<Style> findAll() {
        return this.styleService.findAll();
    }

    @GetMapping("{id}")
    public Style findById(@PathVariable String id) {
        return this.styleService.findById(id);
    }

    @PostMapping
    public Style create(@RequestBody Style style) {
        return this.styleService.create(style);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        this.styleService.deleteById(id);
    }

    @PutMapping("{id}")
    public Style update(@RequestBody Style style) {
        return this.styleService.update(style);
    }
}
