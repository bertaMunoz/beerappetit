package com.example.beerappetit.repositories;

import com.example.beerappetit.models.Country;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CountryRepository extends MongoRepository<Country,String> {


}
