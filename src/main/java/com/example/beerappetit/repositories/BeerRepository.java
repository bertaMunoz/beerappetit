package com.example.beerappetit.repositories;

import com.example.beerappetit.models.Beer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BeerRepository extends MongoRepository<Beer, String> {


}

