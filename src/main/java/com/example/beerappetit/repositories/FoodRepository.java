package com.example.beerappetit.repositories;

import com.example.beerappetit.models.Food;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FoodRepository extends MongoRepository<Food, String> {
}
