package com.example.beerappetit.repositories;

import com.example.beerappetit.models.Color;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ColorRepository extends MongoRepository<Color, String> {
}
