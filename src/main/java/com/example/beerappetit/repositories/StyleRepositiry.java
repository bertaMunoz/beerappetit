package com.example.beerappetit.repositories;

import com.example.beerappetit.models.Style;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StyleRepositiry extends MongoRepository<Style, String> {
}
