package com.example.beerappetit.repositories;

import com.example.beerappetit.models.ERole;
import com.example.beerappetit.models.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role,String> {
    Optional<Role> findByName(ERole name);
}
