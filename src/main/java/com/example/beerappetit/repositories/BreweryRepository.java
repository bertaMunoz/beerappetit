package com.example.beerappetit.repositories;

import com.example.beerappetit.models.Brewery;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BreweryRepository extends MongoRepository<Brewery, String> {


    public Brewery findByName(String name) ;
}