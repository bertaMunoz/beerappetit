package com.example.beerappetit.payload.request;

import java.util.Set;

import javax.validation.constraints.*;

public class RegisterRequest {
    private String username;
    @NotBlank
    @Size(max = 25)
    private String firstname;
    @Size(max = 25)
    @NotBlank
    private String nickname;
    @NotBlank
    @Size(max = 25)
    private String lastname;
    @NotBlank
    @Size(max = 50)
    private String email;
    private Set<String> roles;
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    public String getNickname() {return nickname;}

    public void setNickname(String nickname) {this.nickname = nickname;}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        return this.roles;
    }

    public void setRole(Set<String> roles) {
        this.roles = roles;
    }

    public String getFirstname() {return firstname;}

    public void setFirstname(String firstname) { this.firstname = firstname;}

    public String getLastname() { return lastname;}

    public void setLastname(String lastname) { this.lastname = lastname;}
}
