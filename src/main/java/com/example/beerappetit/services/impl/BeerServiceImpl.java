package com.example.beerappetit.services.impl;

import com.example.beerappetit.dtos.BeerDto;
import com.example.beerappetit.models.Beer;
import com.example.beerappetit.models.Brewery;
import com.example.beerappetit.models.Color;
import com.example.beerappetit.repositories.BeerRepository;
import com.example.beerappetit.repositories.BreweryRepository;
import com.example.beerappetit.services.GenericService;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BeerServiceImpl implements GenericService<Beer> {

    private final BeerRepository repository;
    private final ModelMapper mapper;

    public BreweryRepository breweryRepository;
    public BreweryServiceImpl breweryService;
    public BeerServiceImpl(BeerRepository repository,ModelMapper mapper, BreweryServiceImpl breweryService) {
        this.repository = repository;
        this.mapper = mapper;
        this.breweryService = breweryService;
    }


    @Override
    public List<Beer> findAll() {

        return this.repository.findAll();
    }

    @Override
    public Beer findById(String id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Beer create(Beer entity) {
        Boolean breweryExists = this.breweryService.breweryExists(entity.getBrewery().getId());
        System.out.println(breweryExists);
        if(!breweryExists) {
            Brewery newBrewery = new Brewery(entity.getBrewery().getName());
            this.breweryRepository.save(newBrewery);
            entity.setBrewery(newBrewery);
        }
        return this.repository.save(entity);
    }
    @Override
    public Beer update(Beer entity) {
        return this.repository.save(entity);
    }

    @Override
    public void deleteById(String id) {
        this.repository.deleteById(id);
    }

   /* public Beer addBrewery(String id, Brewery brewery) {
        Beer beer = this.repository.findBy(id).get();
        brewery = this.breweryService.create(brewery);

    }

    */
}
