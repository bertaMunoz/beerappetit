package com.example.beerappetit.services.impl;

import com.example.beerappetit.models.Country;
import com.example.beerappetit.repositories.CountryRepository;
import com.example.beerappetit.services.GenericService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;

import java.util.List;

public class CountryServiceImpl implements GenericService<Country> {

    private final CountryRepository repository;
    private final ModelMapper mapper;

    public CountryServiceImpl(CountryRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public List<Country> findAll() {
        return this.repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public Country findById(String id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Country create(Country entity) {
        return this.repository.save(entity);
    }

    @Override
    public Country update(Country entity) {
        return this.repository.save(entity);
    }

    @Override
    public void deleteById(String id) {
        this.repository.deleteById(id);
    }
}
