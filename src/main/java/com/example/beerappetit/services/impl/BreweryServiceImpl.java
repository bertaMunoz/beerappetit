package com.example.beerappetit.services.impl;

import com.example.beerappetit.models.Brewery;
import com.example.beerappetit.repositories.BeerRepository;
import com.example.beerappetit.repositories.BreweryRepository;
import com.example.beerappetit.services.GenericService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;

import java.util.List;

public class BreweryServiceImpl implements GenericService<Brewery> {

    private final BreweryRepository repository;
    private final ModelMapper mapper;

    public BreweryServiceImpl(BreweryRepository  repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public List<Brewery> findAll() {
        return this.repository.findAll(Sort.by(Sort.Order.asc("name")));
    }

    @Override
    public Brewery findById(String id) {

        return this.repository.findById(id).get();
    }

    @Override
    public Brewery create(Brewery entity) {

        return this.repository.save(entity);
    }

    @Override
    public Brewery update(Brewery entity) {

        return this.repository.save(entity);
    }

    @Override
    public void deleteById(String id) {
        this.repository.deleteById(id);
    }

    public boolean breweryExists(String id) {
        if(this.repository.findById(id)!=null){
            return true;
        } else {
            return false;
        }
    }
}
