package com.example.beerappetit.services.impl;

import com.example.beerappetit.dtos.user.GetProfilDto;
import com.example.beerappetit.models.Beer;
import com.example.beerappetit.models.User;
import com.example.beerappetit.repositories.BeerRepository;
import com.example.beerappetit.repositories.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl {


    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private  UserRepository userRepository;
    private ObjectMapper mapper;
    PasswordEncoder encoder = new BCryptPasswordEncoder();

    public UserServiceImpl(UserRepository userRepository, ObjectMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    public List<User> findAll(){
        return this.userRepository.findAll();
    }

     public GetProfilDto findById(String id) {
        Optional<User> user = this.userRepository.findById(id);
         if (user.isEmpty()) throw new NullPointerException("pas de user");
         return mapper.convertValue(user,GetProfilDto.class);
    }

    public User updateEmail(String id, String email) {
        User user = this.userRepository.findById(id).get();
        user.setEmail(email);
        return this.userRepository.save(user);
    }

    public void deleteById(String id) {
        this.userRepository.deleteById(id);
    }

    public List<Beer> setFavoris(String userId, Beer beer){

        User user = this.userRepository.findById(userId).get();
        List<Beer> favoris =user.getFavoris();
        if(favoris.contains(beer)){
            user.deleteFavori(beer);
        } else {
            user.addFavori(beer);
        }
        this.userRepository.save(user);
        return user.getFavoris();
    }

    public List<Beer> getFavoris(String userId){
        User user = this.userRepository.findById(userId).get();
        return user.getFavoris();
    }

    public List<Beer> setShopList(String userId, Beer beer){

        User user = this.userRepository.findById(userId).get();
        List<Beer> shopList =user.getShopList();
        if(shopList.contains(beer)){
            user.deleteFromShopList(beer);
        } else {
            user.addToShopList(beer);
        }
        this.userRepository.save(user);
        return user.getShopList();
    }

    public List<Beer> getShopList(String userId){
        User user = this.userRepository.findById(userId).get();
        return user.getShopList();
    }

    public int getCountFavori(String userId){
        User user = this.userRepository.findById(userId).get();
        return user.getFavoris().size();
    }

}
