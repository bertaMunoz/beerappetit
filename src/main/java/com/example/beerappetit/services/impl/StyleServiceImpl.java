package com.example.beerappetit.services.impl;

import com.example.beerappetit.models.Style;
import com.example.beerappetit.repositories.StyleRepositiry;
import com.example.beerappetit.services.GenericService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;

import java.util.List;

public class StyleServiceImpl implements GenericService<Style> {

    private final StyleRepositiry repository;
    private final ModelMapper mapper;

    public StyleServiceImpl(StyleRepositiry repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public List<Style> findAll() {
        return this.repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public Style findById(String id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Style create(Style entity) {
        return this.repository.save(entity);
    }

    @Override
    public Style update(Style entity) {
        return this.repository.save(entity);
    }

    @Override
    public void deleteById(String id) {
         this.repository.deleteById(id);
    }
}
