package com.example.beerappetit.services.impl;

import com.example.beerappetit.models.Color;
import com.example.beerappetit.repositories.ColorRepository;
import com.example.beerappetit.services.GenericService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;

import java.util.List;

public class ColorServiceImpl implements GenericService<Color> {

    private final ColorRepository repository;
    private final ModelMapper mapper;

    public ColorServiceImpl(ColorRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public List<Color> findAll() {
        return this.repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public Color findById(String id) {
        return this.repository.findById(id).get();
    }

    @Override
    public Color create(Color entity) {
        return this.repository.save(entity);
    }

    @Override
    public Color update(Color entity) {
        return this.repository.save(entity);
    }

    @Override
    public void deleteById(String id) {
        this.repository.deleteById(id);
    }
}
