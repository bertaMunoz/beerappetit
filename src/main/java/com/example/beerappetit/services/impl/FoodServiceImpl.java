package com.example.beerappetit.services.impl;

import com.example.beerappetit.models.Food;
import com.example.beerappetit.repositories.FoodRepository;
import com.example.beerappetit.services.GenericService;
import org.modelmapper.ModelMapper;

import java.util.List;

public class FoodServiceImpl implements GenericService<Food> {

    private final FoodRepository repo;
    private final ModelMapper mapper;

    public FoodServiceImpl(FoodRepository repo, ModelMapper mapper) {
        this.repo = repo;
        this.mapper = mapper;
    }

    @Override
    public List<Food> findAll() {
        return this.repo.findAll();
    }

    @Override
    public Food findById(String id) {
        return this.repo.findById(id).get();
    }

    @Override
    public Food create(Food entity) {
        return this.repo.save(entity);
    }

    @Override
    public Food update(Food entity) {
        return this.repo.save(entity);
    }

    @Override
    public void deleteById(String id) {
        this.repo.deleteById(id);
    }
}
