package com.example.beerappetit.services;

import com.example.beerappetit.dtos.BeerDto;

import java.util.List;
import java.util.Optional;

public interface GenericService<T> {


    public List<T> findAll();
    public T findById(String id);
    public T create(T entity);
    public T update(T entity);
    public void deleteById(String id);

}
