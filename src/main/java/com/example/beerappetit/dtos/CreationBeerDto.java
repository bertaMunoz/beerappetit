package com.example.beerappetit.dtos;

import com.example.beerappetit.models.Brewery;
import com.example.beerappetit.models.Color;
import com.example.beerappetit.models.Country;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreationBeerDto {

    private String name;
    private String alcool;
    private String img;
    private Color color;
    private Country country;
    private Brewery brewery;
    private String appearance;
    private String aroma;
    private String taste;

    @Override
    public String toString() {
        return "CreationBeerDto{" +
                "name='" + name + '\'' +
                ", alcool='" + alcool + '\'' +
                ", img='" + img + '\'' +
                ", color=" + color +
                ", country=" + country +
                ", brewery='" + brewery + '\'' +
                ", appearance='" + appearance + '\'' +
                ", aroma='" + aroma + '\'' +
                ", taste='" + taste + '\'' +
                '}';
    }
}
