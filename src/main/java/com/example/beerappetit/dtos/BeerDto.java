package com.example.beerappetit.dtos;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@AllArgsConstructor
@NoArgsConstructor
public class BeerDto {

    private String id;
    private String name;
    private String alcool;
    private String img;
    private String color;
    private String country;
    private String brewery;
    private String appearance;
    private String aroma;
    private String taste;

}
