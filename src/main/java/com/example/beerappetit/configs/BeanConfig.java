package com.example.beerappetit.configs;


import com.example.beerappetit.repositories.*;
import com.example.beerappetit.services.impl.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BeanConfig {


    @Bean
    public ModelMapper modelMapper() {

        return new ModelMapper();
    }

    @Bean
    BeerServiceImpl beerService(BeerRepository repository, ModelMapper mapper, BreweryServiceImpl brewServ) {
        return new BeerServiceImpl(repository, mapper, brewServ);
    }

    @Bean
    BreweryServiceImpl breweryService(BreweryRepository repository, ModelMapper mapper) {
        return new BreweryServiceImpl(repository, mapper);
    }

    @Bean
    ColorServiceImpl colorService(ColorRepository repository, ModelMapper mapper) {
        return new ColorServiceImpl(repository, mapper);
    }

    @Bean
    CountryServiceImpl countryService(CountryRepository repository, ModelMapper mapper) {
        return new CountryServiceImpl(repository, mapper);
    }

    @Bean
    StyleServiceImpl styleService(StyleRepositiry repository, ModelMapper mapper) {
        return new StyleServiceImpl(repository, mapper);
        }


    @Bean
    UserServiceImpl userServiceFactory(UserRepository repository, ObjectMapper mapper){
        return new UserServiceImpl(repository,mapper);
    }

    @Bean
    FoodServiceImpl foodServiceFactory(FoodRepository repository, ModelMapper mapper){
        return new FoodServiceImpl(repository, mapper);
    }
}
