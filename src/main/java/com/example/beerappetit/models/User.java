package com.example.beerappetit.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Document(collection = "users")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {
    @Id
    private String id;
    @Size(max = 25)
    @NotBlank
    @NotNull
    private String firstname;
    @Size(max = 25)
    @NotBlank
    private String lastname;
    @Size(max = 50)
    @Email
    @NotBlank
    private String email;
    @Size(max = 120)
    @NotBlank
    private String password;
    @Size(max = 50)
    @Email
    @NotBlank
    private String username;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String avatar;
    @Size(max = 25)
    @NotBlank
    private String nickname;
    @DBRef
    private List<Beer> favoris= new ArrayList<>();
    private List<Beer> shopList= new ArrayList<>();
    private Set<Role> roles =new HashSet<>();

    public User(String email, String password, String username) {
        this.email = email;
        this.password = password;
        this.username = username;
    }

    public User(String firstname, String lastname, String email, String password, String username, String nickname) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.username = email;
        this.nickname = nickname;
        this.createdAt= LocalDateTime.now();
        this.updatedAt=LocalDateTime.now();
    }

    public void setEmail(String email) {
        this.email = email;
        this.username=email;
    }

    public void addFavori(Beer beer){
        this.favoris.add(beer);
    }

    public void deleteFavori(Beer beer){
        this.favoris.remove(beer);
    }

    public void addToShopList(Beer beer){
        this.shopList.add(beer);
    }

    public void deleteFromShopList(Beer beer){
        this.shopList.remove(beer);
    }
}
