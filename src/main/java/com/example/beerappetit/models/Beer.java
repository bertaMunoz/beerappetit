package com.example.beerappetit.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.bind.annotation.CrossOrigin;

@Document
@Data
@CrossOrigin
@AllArgsConstructor
@NoArgsConstructor
public class Beer {

    @Id
    private String id;
    private String name;
    private String alcool;
    private String img;
    @DBRef
    private Color color;
    @DBRef
    private Country country;
    @DBRef
    private Brewery brewery;
    @DBRef
    private Style style;
    private String appearance;
    private String aroma;
    private String taste;

}
