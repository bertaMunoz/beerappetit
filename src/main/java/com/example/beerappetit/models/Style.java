package com.example.beerappetit.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.bind.annotation.CrossOrigin;

@Document
@Data
@CrossOrigin
@AllArgsConstructor
@NoArgsConstructor
public class Style {
    @Id
    private String id;
    private String name;
}
