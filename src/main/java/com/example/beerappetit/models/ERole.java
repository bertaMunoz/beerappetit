package com.example.beerappetit.models;

public enum ERole {
    ROLE_USER,
    ROLE_MEMBER,
    ROLE_ADMIN
}

