package com.example.beerappetit.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Brewery {

    @Id
    private String id;
    private String name;

    public Brewery(String name) {
        this.name = name;
    }
   /* public void addBrewery(String name) {
        this.name.add(name);
    }

    */
}
